'use strict';

angular
  .module('bravoureAngularApp')
  .constant('IMAGE_SIZE', {

      /*/////////////////////////////////

       Media

       //////////////////////////////////*/

      /*  Used by:
       - Angular Block Image
       */
      'block_image_small': {
          'mobile': {
              'height': 300,
              'width': 600
          },
          'tablet': {
              'height': 500,
              'width': 1000
          },
          'desktop': {
              'height': 500,
              'width': 1000
          },
          'desktop_big': {
              'height': 750,
              'width': 1500
          }
      },

      /*  Used by:
       - Angular Block Image
       */
      'block_image_medium': {
          'mobile': {
              'height': 400,
              'width': 600
          },
          'tablet': {
              'height': 500,
              'width': 1000
          },
          'desktop': {
              'height': 500,
              'width': 1000
          },
          'desktop_big': {
              'height': 750,
              'width': 1500
          }
      },

      /*  Used by:
       - Angular Block Image
       */
      'block_image_large': {
          'mobile': {
              'height': 350,
              'width': 700
          },
          'tablet': {
              'height': 500,
              'width': 1000
          },
          'desktop': {
              'height': 500,
              'width': 1000
          },
          'desktop_big': {
              'height': 750,
              'width': 1500
          }
      },

      /*  Used by:
       - Angular Block Image
       */
      'block_image_full': {
          'mobile': {
              'height': 295,
              'width': 768
          },
          'tablet': {
              'height': 295,
              'width': 768
          },
          'desktop': {
              'height': 381,
              'width': 992
          },
          'desktop_big': {
              'height': 450,
              'width': 1170
          }
      },

      /*  Used by:
       - Angular Block Video
       */
      'block_video': {
          'mobile': {
              'height': 295,
              'width': 768
          },
          'tablet': {
              'height': 295,
              'width': 768
          },
          'desktop': {
              'height': 381,
              'width': 992
          },
          'desktop_big': {
              'height': 450,
              'width': 1170
          }
      },

      /*  Used by:
       - Angular Block Soundcloud
       */
      'block_soundcloud': {
          'mobile': {
              'height': 160,
              'width': 160
          },
          'tablet': {
              'height': 200,
              'width': 200
          },
          'desktop': {
              'height': 240,
              'width': 240
          },
          'desktop_big': {
              'height': 180,
              'width': 180
          }
      },

      /*  Used by:
       - Block Image Slider
       */
      'block_image_slider': {
          'mobile': {
              'height': 540,
              'width': 540
          },
          'tablet': {
              'height': 900,
              'width': 900
          },
          'desktop': {
              'height': 450,
              'width': 450
          },
          'desktop_big': {
              'height': 550,
              'width': 550
          }
      },

      /*/////////////////////////////////

       Content

       //////////////////////////////////*/

      /*  Used by:
       - Angular Block Content
       */
      'block_content': {
          'mobile': {
              'height': 360,
              'width': 750
          },
          'tablet': {
              'height': 600,
              'width': 600
          },
          'desktop': {
              'height': 450,
              'width': 450
          },
          'desktop_big': {
              'height': 500,
              'width': 500
          }
      },

      /*  Used by:
       - Block CTA
       */
      'block_cta': {
          'mobile': {
              'height': 300,
              'width': 600
          },
          'tablet': {
              'height': 340,
              'width': 680
          },
          'desktop': {
              'height': 250,
              'width': 500
          },
          'desktop_big': {
              'height': 250,
              'width': 500
          }
      },

      /*/////////////////////////////////

       Lists

       //////////////////////////////////*/

      /*  Used by:
       - Block Item List
       */
      'block_item_general': {
          'mobile': {
              'height': 360,
              'width': 750
          },
          'tablet': {
              'height': 360,
              'width': 360
          },
          'desktop': {
              'height': 640,
              'width': 640
          },
          'desktop_big': {
              'height': 640,
              'width': 640
          }
      },

      /*  Used by:
       - Block Item Grid
       */
      'block_item_grid': {
          'mobile': {
              'height': 360,
              'width': 750
          },
          'tablet': {
              'height': 360,
              'width': 360
          },
          'desktop': {
              'height': 640,
              'width': 640
          },
          'desktop_big': {
              'height': 640,
              'width': 640
          }
      },

      /*  Used by:
       - Block Item Masonry
       */
      'block_item_masonry': {
          'mobile': {
              'height': 360,
              'width': 750
          },
          'tablet': {
              'height': 360,
              'width': 360
          },
          'desktop': {
              'height': 640,
              'width': 640
          },
          'desktop_big': {
              'height': 640,
              'width': 640
          }
      },

      /*  Used by:
       - Block Item Accordion
       */
      'block_item_accordion': {
          'mobile': {
              'height': 360,
              'width': 750
          },
          'tablet': {
              'height': 300,
              'width': 300
          },
          'desktop': {
              'height': 450,
              'width': 450
          },
          'desktop_big': {
              'height': 500,
              'width': 500
          }
      },

      /*  Used by:
       - Block Item Usp
       */
      'block_item_usp': {
          'mobile': {
              'height': 60,
              'width': 60
          },
          'tablet': {
              'height': 60,
              'width': 60
          },
          'desktop': {
              'height': 30,
              'width': 30
          },
          'desktop_big': {
              'height': 30,
              'width': 30
          }
      },

      /*  Used by:
       - Block Item Logo
       */
      'block_item_logo': {
          'mobile': {
              'height': 300,
              'width': 600
          },
          'tablet': {
              'height': 340,
              'width': 680
          },
          'desktop': {
              'height': 350,
              'width': 700
          },
          'desktop_big': {
              'height': 350,
              'width': 700
          }
      },

      'block_item_slide': {
          'mobile': {
              'height': 540,
              'width': 640
          },
          'tablet': {
              'height': 900,
              'width': 640
          },
          'desktop': {
              'height': 450,
              'width': 350
          },
          'desktop_big': {
              'height': 550,
              'width': 400
          }
      },


      /*/////////////////////////////////

       Templates

       //////////////////////////////////*/

      /*  Used by:
       - Header Template 1
       */
      'header_template_1': {
          'mobile': {
              'height': 470,
              'width': 695
          },
          'tablet': {
              'height': 530,
              'width': 784
          },
          'desktop': {
              'height': 302,
              'width': 1000
          },
          'desktop_big': {
              'height': 502,
              'width': 1500
          }
      },

      /*  Used by:
       - Header Template 2
       */
      'header_template_2': {
          'mobile': {
              'height': 470,
              'width': 695
          },
          'tablet': {
              'height': 530,
              'width': 784
          },
          'desktop': {
              'height': 750,
              'width': 1500
          },
          'desktop_big': {
              'height': 502,
              'width': 1500
          }
      },

      /*  Used by:
       - Header Template Landing
       */
      'header_template_landing_page': {
          'mobile': {
              'height': 550,
              'width': 375
          },
          'tablet': {
              'height': 800,
              'width': 768
          },
          'desktop': {
              'height': 750,
              'width': 1220
          },
          'desktop_big': {
              'height': 750,
              'width': 1400
          }
      },

      /*/////////////////////////////////

       Other

       //////////////////////////////////*/

      /*  Used by:
       - Block Item Artist
       */
      'artist_profile': {
          'mobile': {
              'height': 650,
              'width': 650
          },
          'tablet': {
              'height': 400,
              'width': 400
          },
          'desktop': {
              'height': 600,
              'width': 600
          },
          'desktop_big': {
              'height': 600,
              'width': 600
          }
      },

      /*  Used by:
       - Author
       */
      'author': {
          'mobile': {
              'height': 120,
              'width': 120
          },
          'tablet': {
              'height': 140,
              'width': 140
          },
          'desktop': {
              'height': 140,
              'width': 140
          },
          'desktop_big': {
              'height': 140,
              'width': 140
          }
      },
  });
